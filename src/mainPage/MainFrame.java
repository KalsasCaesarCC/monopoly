package mainPage;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.*;

import common.Players;
import common.Properties;
import common.PublicData;
import gamePanels.MainPanel;
import gamePanels.PlayerPanel;
import gamePanels.PropertyPanel;

/**
 * Game main frame 
 * @author Cheng Chen 
 *
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {	
	private int numOfPly;
	private int numOfPro;
	private MainPanel mainPanel;
	private PropertyPanel propertyPanel;
	private PlayerPanel playerPanel;
	private ControlPanel controlPanel;
	private Players players[]; 
	private Properties property[];
	private Dimension frameSize = new Dimension(1900, 1040);
	
	// test2
	
	/**
	 * Create game main frame 
	 * @param nPly number of player
	 */
	public MainFrame(int nPly) {
		numOfPly = nPly;
		numOfPro = 28;
		
		setTitle("Monopoly");
		setSize(frameSize);
		setLayout(null);
		
		// initiate all players (players must initiated before panels)
		players = new Players[numOfPly];
		for(int i=0; i<numOfPly; i++) {
			players[i] = new Players(PublicData.plyName[i]);
		}
		
		// initiate all properties 
		property = new Properties[numOfPro];
		for(int i=0; i<numOfPro; i++) {
			property[i] = new Properties((String)PublicData.proInfo[i][PublicData.proIndex.NAME.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.PRICE.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.RENT.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE1.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE2.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE3.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE4.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOTEL.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HCOST.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.BLOCK.ordinal()],
					   (String)PublicData.proInfo[i][PublicData.proIndex.COLOR.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.NHOUSE.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.NHOTEL.ordinal()],
					   (String)PublicData.proInfo[i][PublicData.proIndex.OWNER.ordinal()]);
		}
		
		// initiate all panels
		mainPanel = new MainPanel(10, 10, 985, 985, numOfPly);
		propertyPanel = new PropertyPanel(1015, 10, 400, 700);
		playerPanel = new PlayerPanel(1425, 10, 445, 700);
		controlPanel = new ControlPanel(1015, 720, 855, 275);
		
		// add panels
		add(mainPanel);
		add(propertyPanel);
		add(playerPanel);
		add(controlPanel);
	}
	
	/**
	 * Creates Control Panel, manage all game operations 
	 * @author Cheng Chen
	 *
	 */
	public class ControlPanel extends JPanel {	
		private JLabel currentPly, numOfHouse, numOfHotel, numOfPro, dice1, dice2;
		private JComboBox<String> proSelection;
		private JButton btnGo, btnSellHouse;
		private ImageIcon imgDice1, imgDice2;
		private int playerNo;
		private boolean endTurn = false; 
		
		/**
		 * Setup ControlPanel 
		 * @param x position in main frame 
		 * @param y position in main frame
		 * @param width panel width
		 * @param height panel height
		 */
		public ControlPanel(int x, int y, int width, int height) {
			setBounds(x, y, width, height);
			setBorder(BorderFactory.createLineBorder(Color.black, 3));
			setBackground(new Color(169,209,142));
			setLayout(null);
			
			initComponents();
			
			add(currentPly);
			add(numOfHouse);
			add(numOfHotel);
			add(numOfPro);
			add(dice1);
			add(dice2);
			add(proSelection);
			add(btnSellHouse);
			add(btnGo);
		}
		
		/**
		 * Initiate all components 
		 */
		private void initComponents() {	
			dice1 = new JLabel();
			dice2 = new JLabel();
			currentPly = new JLabel("Current Player: Player 1");
			numOfHouse = new JLabel("Rest Number of House: 32");
			numOfHotel = new JLabel("Rest Number of Hotel: 12");
			numOfPro = new JLabel("Rest Number of Free Property: 28");
			proSelection = new JComboBox<String>();
			btnGo = new JButton("GO");
			btnSellHouse = new JButton("Sell House");
			
			currentPly.setBounds(50, 30, 250, 50);
			numOfHouse.setBounds(50, 90, 200, 50);
			numOfHotel.setBounds(50, 120, 200, 50);
			numOfPro.setBounds(50, 150, 250, 50);
			proSelection.setBounds(480, 30, 210, 30);
			dice1.setBounds(650, 170, 80, 80);
			dice2.setBounds(740, 170, 80, 80);
			
			btnSellHouse.setBounds(700, 30, 120, 30);
			
			btnGo.setBounds(540, 190, 100, 60);
			btnGo.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// Dice
					if(endTurn) {
						endTurn = false;
						btnGo.setText("GO");
						plySwitch(playerNo);
					} else {
						endTurn = true; 
						btnGo.setText("End Turn");
						plyMove(dice(), playerNo);
						
						playerNo++;
						if(playerNo == numOfPly)
							playerNo = 0;
					}			
				}
			});
			
			// initial info of control panel 
			setPubInfo(players[0].getName(), 32, 12, 28);
			showDice(1, 1);			
		}
		
		/**
		 * Set public information of Panel 
		 * @param ply name of player
		 * @param nHouse total house remaining 
		 * @param nHotel total hotel remaining
		 * @param nPro total free properties remaining 
		 */
		public void setPubInfo(String ply, int nHouse, int nHotel, int nPro) {
			proSelection.addItem("China");
			proSelection.addItem("USA");
			proSelection.addItem("Ireland");
			
			currentPly.setText("Current Player: " + ply);
			currentPly.setFont(new Font("currentPly", Font.ITALIC, 20));
			currentPly.setForeground(Color.red);
			
			numOfHouse.setText("House Remaining: " + String.valueOf(nHouse));
			numOfHouse.setFont(new Font("numOfHouse", Font.ITALIC, 15));
			
			numOfHotel.setText("Hotel Remaining: " + String.valueOf(nHotel));
			numOfHotel.setFont(new Font("numOfHotel", Font.ITALIC, 15));
			
			numOfPro.setText("Property Remaining: " + String.valueOf(nPro));
			numOfPro.setFont(new Font("currentPly", Font.ITALIC, 15));
		}
		
		/**
		 * Show dice value 
		 * @param dicePoint1 dice 1
		 * @param dicePoint2 dice 2
		 */
		public void showDice(int dicePoint1, int dicePoint2) {
			imgDice1 = new ImageIcon("images/Dice/" + String.valueOf(dicePoint1) + ".png");
			imgDice1.setImage(imgDice1.getImage().getScaledInstance(dice1.getWidth(), 
					dice1.getHeight(), Image.SCALE_DEFAULT ));
			
			imgDice2 = new ImageIcon("images/Dice/" + String.valueOf(dicePoint2) + ".png");
			imgDice2.setImage(imgDice2.getImage().getScaledInstance(dice2.getWidth(), 
					dice2.getHeight(), Image.SCALE_DEFAULT ));
			
			dice1.setIcon(imgDice1);
			dice2.setIcon(imgDice2);
		}
		
		/**
		 * Return dice value  
		 * @return sum of 2 dice results 
		 */
		public int dice() {
			int result1 = new Random().nextInt(6) + 1;
			int result2 = new Random().nextInt(6) + 1;
			showDice(result1, result2);
			//System.out.println("result1: " + result1);
			//System.out.println("result2: " + result2);
			return result1 + result2;
		}
		
		/**
		 * Move the player 
		 * @param dicePoint how many blocks the player should push 
		 * @param plyNo number of current player
		 */
		public void plyMove(int dicePoint, int plyNo) {
			int block = players[plyNo].blockCalculation(dicePoint);
			mainPanel.plyCalculation(block, plyNo);
			blockActivity(block, plyNo);
		}
		
		/**
		 * Switch to next player 
		 * @param plyNo number of current player
		 */
		public void plySwitch(int plyNo) {
			Object[] plyInfo = (Object[]) players[plyNo].getPlyInfo();		
			playerPanel.setPlyInfo((String)plyInfo[0], (int)plyInfo[1], (int)plyInfo[2], 
									(int)plyInfo[3], (boolean)plyInfo[4]);
		}
		
		/* --------------------------------Main Logic Functions--------------------------------*/
		
		/**
		 * Execute activities on specific block
		 * @param block block No. of current player
		 * @param plyNo number of current player
		 */
		public void blockActivity(int block, int plyNo) {
			switch(block) {
				case 4:
				case 13:
				case 28:
					// draw chance card
					break;
				case 8:
				case 24:
				case 33:
					// draw fate card
					break;
				case 15:
				case 35:
					// pay tax
					break;
				case 30:
					// go to jail
					break;
				default:
					// property activity
					break;
			}
		}
		
		/**
		 * Purchase a property 
		 * @param block block No. of current player
		 * @param plyNo number of current player
		 */
		public void proPurchase(int block, int plyNo) {
			
		}
		/* ---------------------------------------------------------------------------------------*/
	}
}
