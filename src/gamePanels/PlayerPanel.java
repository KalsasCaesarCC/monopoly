package gamePanels;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import common.PublicData;

/**
 * Panel of players information 
 * @author Cheng Chen
 *
 */
@SuppressWarnings("serial")
public class PlayerPanel extends JPanel {
	private JLabel plyAvatar, plyName, plyCash, plyJCard, plyPro, prison;
	private ImageIcon imgAvatar;
	
	/**
	 * Creates player panel 
	 * @param x position in main frame 
	 * @param y position in main frame 
	 * @param width panel width 
	 * @param height panel width
	 */
	public PlayerPanel(int x, int y, int width, int height) {

		setBounds(x, y, width, height);
		setBorder(BorderFactory.createLineBorder(Color.black, 3));
		setBackground(new Color(169,209,142));
		setLayout(null);
		
		initComponents();
		
		add(plyAvatar);
		add(plyName);
		add(plyCash);
		add(plyJCard);
		add(plyPro);
		add(prison);
	}
	
	/**
	 * Initiate all components 
	 */
	private void initComponents() {
		prison = new JLabel("", JLabel.CENTER);
		plyAvatar = new JLabel();
		plyName = new JLabel("Player 1", JLabel.CENTER);
		plyCash = new JLabel("Cash: 1000 ��");
		plyJCard = new JLabel("Jail Free Card: 0");
		plyPro = new JLabel("Property Owned: 0");
		
		plyAvatar.setBounds(147, 40, 150, 150);
		plyName.setBounds(147, 200, 150, 30);
		plyCash.setBounds(50, 250, 200, 30);
		plyJCard.setBounds(50, 275, 200, 30);
		plyPro.setBounds(50, 300, 200, 30);
		prison.setBounds(50, 330, 200, 60);
		
		setPlyInfo(PublicData.plyName[0], PublicData.plyInitCash, 0, 0, false);
		
	}
	
	/**
	 * Set players information 
	 * @param name name of player
	 * @param cash number of cash owned 
	 * @param jcard number of jail card owned
	 * @param nPro number of properties owned 
	 * @param isPrison is the player in jail  
	 */
	public void setPlyInfo(String name, int cash, int jcard, int nPro, boolean isPrison) {
		imgAvatar = new ImageIcon("images/Avatars/" + name + ".png");
		imgAvatar.setImage(imgAvatar.getImage().getScaledInstance(plyAvatar.getWidth(), 
				plyAvatar.getHeight(), Image.SCALE_DEFAULT ));
		
		plyAvatar.setIcon(imgAvatar);
		plyAvatar.setBorder(BorderFactory.createLineBorder(new Color(0,0,205), 3));
		
		plyName.setText(name);
		plyName.setFont(new Font("plyName", Font.BOLD, 20));
		plyName.setForeground(Color.red);
		
		plyCash.setText("Cash: " + String.valueOf(cash) + " ��");
		plyCash.setFont(new Font("plyCash", Font.ITALIC, 15));
		
		plyJCard.setText("Jail Free Card: " + String.valueOf(jcard));
		plyJCard.setFont(new Font("plyJCard", Font.ITALIC, 15));
		
		plyPro.setText("Property Owned: " + String.valueOf(nPro));
		plyPro.setFont(new Font("plyPro", Font.ITALIC, 15));
		
		prison.setText(isPrison == true ? "IN JAIL!!!" : "");
		prison.setFont(new Font("prison", Font.ITALIC, 40));
		prison.setForeground(Color.red);
		prison.setBorder(isPrison == true ? BorderFactory.createLineBorder(Color.red, 4) : null);
	}
}

