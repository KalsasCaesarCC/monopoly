package startPage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.border.Border;

import mainPage.MainFrame;

/**
 * The Start Game page.
 * @author Cheng Chen
 *  
 */

@SuppressWarnings("serial")
public class StartGame extends JFrame {
	private Dimension frameSize = new Dimension(1500, 840);
	private Image startBackground = new ImageIcon("images/startPage.jpg").getImage();
	
	/**
	 * Creates the start game page 
	 */
	public StartGame() {
		setSize(frameSize);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setIconImage(startBackground);
	    setBackground();
	    setComponents();
	    setVisible(true);
	}
	
	/**
	 * Setup page background image
	 */
	private void setBackground() {
		ImagePanel imagePanel = new ImagePanel(frameSize, startBackground);
		setContentPane(imagePanel);
	}
	
	/**
	 * Setup page components 
	 */
	private void setComponents() {
		Border border = BorderFactory.createLineBorder(new Color(0,255,0), 5);
		
		// create components
		JTextField numOfPlayer = new JTextField("Enter the number of players (2-6)");
		JButton startIcon = new JButton("Start Game");
		
		// player number selection field setup
		numOfPlayer.setBounds(550, 360, 400, 80);
		numOfPlayer.setFont(new Font("numOfPlayer", Font.BOLD, 24));
		numOfPlayer.setHorizontalAlignment(JTextField.CENTER);
		numOfPlayer.setForeground(new Color(0,128,255));
		numOfPlayer.setBorder(border);
		
		// start icon setup
		startIcon.setBounds(650, 450, 200, 50);
		startIcon.setFont(new Font("startIcon", Font.BOLD, 24));
		startIcon.setBackground(new Color(0,255,0));
		startIcon.setForeground(Color.white);
		
		// listener
		startIcon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                startGame();
            }
	    });
		
		// add components to pane 
		this.getContentPane().add(numOfPlayer);
		this.getContentPane().add(startIcon);
	}
	
	/**
	 * Creates game main frame 
	 */
	public void startGame() {
		MainFrame mainPage = new MainFrame(4);
		mainPage.setVisible(true);
	}
}
