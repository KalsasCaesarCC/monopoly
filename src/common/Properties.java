package common;

/**
 * Represents a property 
 * @author Cheng Chen 
 *
 */
public class Properties {
	private String Name, Owner, ColorGroup;
	private int numOfHouse, numOfHotel, Price, Rent, Block,
	            House1, House2, House3, House4, Hotel, Hcost;
	
	/**
	 * Creates a property 
	 * @param name name of property 
	 * @param price property fee
	 * @param rent property rent
	 * @param house1 rent with 1 house
	 * @param house2 rent with 2 houses
	 * @param house3 rent with 3 houses
	 * @param house4 rent with 4 houses
	 * @param hotel rent with hotel
	 * @param price of each house/hotel
	 * @param block block number on chess board 
	 * @param cg color group 
	 * @param nHouse number of house built
	 * @param nHotel number of hotel built
	 * @param owner name of owner 
	 */
	public Properties(String name, int price, int rent, int house1,
			          int house2, int house3, int house4, int hotel,
			          int hcost, int block, String cg, int nHouse, 
			          int nHotel, String owner) {
		
		// static value
		Name = name;
		Price = price;
		Rent = rent;
		House1 = house1;
		House2 = house2;
		House3 = house3;
		House4 = house4;
		Hotel = hotel;
		Hcost = hcost;
		Block = block;
		ColorGroup = cg;
		
		// dynamic value
		Owner = owner;
		numOfHouse = nHouse;
		numOfHotel = nHotel;
	}
}

