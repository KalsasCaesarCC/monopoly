package common;

/**
 * Represents a player 
 * @author Cheng Chen
 *
 */
public class Players {
	private String Name;
	private int Cash, JailCard, numOfPro, Block;
	private boolean isPrision;
	
	/**
	 * Create a player 
	 * @param name name of player 
	 */
	public Players(String name) {
		Name = name;
		Cash = PublicData.plyInitCash;
		JailCard = 0;
		numOfPro = 0;
		isPrision = false;
		Block = 0;
	}
	
	/**
	 * Calculate position of player, return the 
	 * number of block where the player should
	 * be  
	 * @param dicePoint dice point
	 * @return number of block 
	 */
	public int blockCalculation(int dicePoint) {
		Block += dicePoint;
		
		// go to jail
		if(Block == 30) {
			Block = 10;
		} else if(Block > 39) {
			Block -= 40;
			addCash(PublicData.salary);
		}
		
		return Block;
	}
	
	/**
	 * Return players name 
	 * @return name of player 
	 */
	public String getName() {
		return Name;
	}
	
	/**
	 * Return players information
	 * @return player information 
	 */
	public Object getPlyInfo() {
		Object[] plyInfo = {Name, Cash, JailCard, numOfPro, isPrision};
		return plyInfo;
	}
	
	/**
	 * Add players cash
	 * @param cash value of cash in 
	 */
	public void addCash(int cash) {
		Cash = Cash + cash;
	}
	
	/**
	 * Reduce players cash
	 * @param cash value of cash out
	 */
	public void reduceCash(int cash) {
		Cash = Cash - cash;
	}
}
